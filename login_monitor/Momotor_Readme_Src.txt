1. ajouter la ligne suivante dans /etc/pam.d/common-session
session optional         pam_exec.so    /usr/local/bin/pam-xutop

2. dans kpad .bashrc ajoute
MOUFILE="/tmp/kpad130612.log"
alias plop="touch $MOUFILE"

3. backuper le bashrc
cp /home/kpad/.bashrc /home/kpad/.bashrc.backup
cp /home/kpad/.bashrc /home/kpad/.bashrc.backup.2

4. dans le bashrc du root ajouter
alias unplop='cp /home/kpad/.bashrc.backup.2 /home/kpad/.bashrc'

5. creer le fichier /usr/local/bin/pam-xutop et 

chmod 700 /usr/local/bin/pam-xutop


#!/bin/sh

[ "$PAM_TYPE" = "open_session" ] || exit 0
{
  echo "User: $PAM_USER"
  echo "Ruser: $PAM_RUSER"
  echo "Rhost: $PAM_RHOST"
  echo "Service: $PAM_SERVICE"
  echo "TTY: $PAM_TTY"
  echo "Date: `date`"
  echo "Server: `uname -a`"
  echo "----------------last----------------"
  last | head -n 10
} | mail -s "[MONITOR][`hostname -s`] LoginInfo $PAM_SERVICE login: $PAM_USER" kryss.col@gmail.com &

[ "$PAM_USER" = "kpad" ] || exit 0
(
MOUFILE="/tmp/kpad130612.log"
sleep 30
if [ -f $MOUFILE ]
then
    rm -rf $MOUFILE
    exit 0;
fi
mv /home/kpad/.bashrc /home/kpad/.bashrc.backup
echo "exit" >> /home/kpad/.bashrc
kill -9 `ps -u kpad -o "pid="`
)&


