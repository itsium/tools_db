#!/bin/sh

# VAR 

SERV_NAME="demo.itsium.cn"

# END 
apt-get update
apt-get -y upgrade
echo $SERV_NAME > /etc/hostname
hostname -F /etc/hostname
# CHANGE TIMEZONE
sudo dpkg-reconfigure tzdata

# reboot
apt-get -y install ntp ntpdate emacs23-nox

# install ruby dep for build from source
apt-get -y build-dep ruby1.9.3 rails

apt-get -y install libmysql++-dev libsqlite3-dev
apt-get -y install imagemagick

# DOWNLOAD ruby source and compile
# $ mkdir ruby-src && cd ruby-src
# $ wget ftp://ftp.ruby-lang.org//pub/ruby/1.9/ruby-1.9.2-p0.tar.gz
# $ tar -zxvf ruby-1.9.2-p0.tar.gz && cd ruby-1.9.2-p0
# $ ./configure && make && sudo make install

sudo ln -s /usr/local/bin/ruby /usr/bin/ruby
echo 'gem: --no-rdoc --no-ri' >> ~/.gemrc
gem update --system
sudo gem install rails --no-rdoc --no-ri
# sudo gem install passenger
# sudo apt-get install libcurl4-openssl-dev apache2-mpm-worker apache2-threaded-dev libapr1-dev libaprutil1-dev
# sudo passenger-install-apache2-module

# The Apache 2 module was successfully installed.
# Please edit your Apache configuration file, and add these lines:

#    LoadModule passenger_module /usr/local/lib/ruby/gems/1.9.1/gems/passenger-4.0.10/buildout/apache2/mod_passenger.so
#    PassengerRoot /usr/local/lib/ruby/gems/1.9.1/gems/passenger-4.0.10
#    PassengerDefaultRuby /usr/local/bin/ruby

# After you restart Apache, you are ready to deploy any number of Ruby on Rails
# applications on Apache, without any further Ruby on Rails-specific
# configuration!

# Press ENTER to continue.

# --------------------------------------------
# Deploying a Ruby on Rails application: an example
# Suppose you have a Rails application in /somewhere. Add a virtual host to your
# Apache configuration file and set its DocumentRoot to /somewhere/public:

   # <VirtualHost *:80>
   #    ServerName www.yourhost.com
   #    # !!! Be sure to point DocumentRoot to 'public'!
   #    DocumentRoot /somewhere/public
   #    <Directory /somewhere/public>
   #       # This relaxes Apache security settings.
   #       AllowOverride all
   #       # MultiViews must be turned off.
   #       Options -MultiViews
   #    </Directory>
   # </VirtualHost>



