#!/bin/sh

# funciotns

promptyn () {
    while true; do
        read -p "$1 " yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer y or n.";;
        esac
    done
}

# main

if [ `id -u` != "0" ]; then
  echo "[+] You have to run this script with root privilege !"
  exit 1
fi

curl -v 2> /dev/null
if [ $? != "2" ]; then
  echo "[+] Please install curl before using this script !"
        exit 1
fi

apt-get -y install libpcre3-dev libssl-dev
apt-get -y build-dep nginx

NGINX_VERSION=`curl http://nginx.org/ 2> /dev/null | grep -B1 stable | grep "nginx-" | head -n 1 | sed "s/.*\(nginx[^<]*\)<\/a>/\1/g"`
CUR_LINK="http://nginx.org/download/"$NGINX_VERSION".tar.gz"
CUR_FILE=`echo $CUR_LINK | cut -d "/" -f 5`
if promptyn "Do you want to install"$CUR_LINK; then
    echo "[+] Resume job."
else
    echo "[+] Exit."
    exit 1
fi
echo "[+] Download Nginx source file $CUR_FILE"
cd /usr/local/src/
wget $CUR_LINK
echo "[+] Umcompress tarball to /usr/local/src/"
tar -zxvf $CUR_FILE 2> /dev/null > /dev/null
if [ $? != "0" ] ; then
    echo "[Error] no file found !"
  exit 1
fi
rm -rf $CUR_FILE
EXTRA_DIR="/usr/local/src/"`ls -p | grep "/" | grep nginx- | head -n 1`
apt-get -v 2> /dev/null > /dev/null
if [ $? = "0" ] ; then
  echo "[+] apt-get found, installing dependancies !"
  apt-get -y build-dep nginx
  HAS_APT_GET='1'
fi

if [ $HAS_APT_GET ] ; then
  cd $EXTRA_DIR
  echo "[+] Compiling nginx please wait !"
  cat src/http/ngx_http_header_filter_module.c  | sed "s/static char ngx_http_server_string.*/static char ngx_http_server_string[] = \"Server: MooOOoOOoo v 2.0\" CRLF;/g" | sed "s/static char ngx_http_server_full_string.*/static char ngx_http_server_full_string[] = \"Server: MooOOoOOoo v 2.0\" CRLF;/g" > header.tmp
  mv -f header.tmp src/http/ngx_http_header_filter_module.c
  ./configure --prefix=/opt/nginx --user=www-data --group=www-data --with-http_ssl_module --without-http_scgi_module --without-http_uwsgi_module --without-http_fastcgi_module && make && make install

  echo "[+] nginx installed !"
else
  echo "[+] Your OS : `uname -o` is not supported yet ! Please contact myass@isaway.com"
fi

# /etc/init/nginx.conf File

# # nginx

# description "nginx http daemon"
# author "Philipp Klose <me@'thisdomain'.de>"

# start on (filesystem and net-device-up IFACE=lo)
# stop on runlevel [!2345]

# env DAEMON=/opt/nginx/sbin/nginx
# env PID=/opt/nginx/logs/nginx.pid

# expect fork
# respawn
# respawn limit 10 5
# #oom never

# pre-start script
#  $DAEMON -t
#  if [ $? -ne 0 ]
#  then exit $?
#  fi
# end script

# exec $DAEMON

# change conf to server_tokens off
 # change 404 and 50x error page

# http://wiki.nginx.org/Modules
# http://wiki.nginx.org/3rdPartyModules
# http://wiki.nginx.org/Configuration