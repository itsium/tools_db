#!/bin/sh

# VAR 

SERV_NAME="demo.itsium.cn"

# END 
apt-get update
apt-get -y upgrade
echo $SERV_NAME > /etc/hostname
hostname -F /etc/hostname
# CHANGE TIMEZONE
sudo dpkg-reconfigure tzdata

# reboot
apt-get -y install ntp ntpdate emacs23-nox

# install ruby dep for build from source
apt-get -y build-dep ruby1.9.3 rails

apt-get -y install libmysql++-dev libsqlite3-dev
apt-get -y install imagemagick

./install_ruby2.sh

# install nginx
./install_nginx_from_source.sh

## Now we need a puma config file: config/puma.rb

# rails_env = ENV['RAILS_ENV'] || 'development'

# threads 4,4

# bind  "unix:///var/run/imybox.sock"
# pidfile "/srv/appname/tmp/puma.pid"
# state_path "/srv/apps/tmp/puma.state"

# activate_control_app

## get upstart script from https://github.com/puma/puma/tree/master/tools/jungle/upstart

# The script expects:
# a config file to exist under config/puma.rb in your app. E.g.: /home/apps/my-app/config/puma.rb.
# a temporary folder to put the PID, socket and state files to exist called tmp/puma. E.g.: /home/apps/my-app/tmp/puma. Puma will take care of the files for you.
# You can always change those defaults by editing the scripts.

# !!! Before starting...

# You need to customise puma.conf to:

# Set the right user your app should be running on unless you want root to execute it!
# Look for setuid apps and setgid apps, uncomment those lines and replace apps to whatever your deployment user is.
# Replace apps on the paths (or set the right paths to your user's home) everywhere else.
# Uncomment the source lines for rbenv or rvm support unless you use a system wide installation of Ruby.