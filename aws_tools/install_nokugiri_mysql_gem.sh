#!/bin/sh

# Nokugiri
sudo apt-get -y install  libxml2-dev libxslt-dev && sudo gem install nokogiri
# Mysql
sudo apt-get -y install libmysql-ruby libmysqlclient-dev && sudo gem install mysql2

sudo apt-get -y install imagemagick libmagickwand-dev libdjvulibre-dev libjpeg-dev libtiff-dev libwmf-dev libmagickcore-dev libmagickwand-dev libmagick++-dev