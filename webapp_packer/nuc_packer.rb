#!/usr/bin/env ruby
$LOAD_PATH << 'vendor/cache'
require 'nokogiri'
require 'open-uri'
require 'ruby-debug'

if ARGV.count != 2 && ARGV.count != 3
	puts "Usage : webapp_packer.rb proj_name host_server [server_dest]"
	exit 1
else
	$proj = ARGV[0]
	$server = ARGV[1]
	if ARGV.count == 3
		$server_dest = ARGV[2]
	else
		$server_dest = $server
	end
	puts "[INFO]Start download \n\tProj:#{$proj}\n\tServer:#{$server}\n\tWebservice Server:#{$server_dest}"
end



# GLOBAL VAR
# $server = 'http://moa.itsium.cn/'#'http://moa.itsium.cn/' # ! termited with /
# $server_dest = 'http://moa.itsium.cn/' # $server
# $proj = 'moa'


# css_rep_pattern 
#    - every url that match :url_patern will be download is :dl is set to true 
#      when :dl is true :url_prefix must content server_name + remote_path
#    - when :match is set every match will be replaced with :replace   
   


$css_rep_pattern = [
{:match => /url\([\"\']?\/fonts[^\)]*/, :dl => true,
	:url_patern => /url\([\"\']?(\/font[^\)\"\']*)/, :url_prefix => "#{$server}",
	:dl_prefix => "assets/"},
{:match => /url\([\"\']?\/assets[^\)]*/, :dl => false},

{:match => /url\([\"\']?images[^\)]*/, :dl => true,
        :url_patern => /url\([\"\']?(images[^\)\"\']*)/, :url_prefix => "#{$server}assets/",
        :dl_prefix => "assets/"},
{:match => /assets\/partners-logo.jpg/, :dl => true,
         :url_patern => /assets\/partners-logo.jpg/, :url_prefix => "#{$server}", 
         :replace => "partners-logo.jpg"},

{:match => /\/assets\/images\/loader-black.gif/, :dl => true,
         :url_patern => /\/assets\/images\/loader-black.gif/, :url_prefix => "#{$server}", 
         :replace => "assets/images/loader-black.gif"},
{:match => /assets\/itsium\-logo\@2x\.jpg/, :url_patern => /assets\/itsium\-logo\@2x\.jpg/,
	:dl => true, :dl_prefix => "", :url_prefix => "#{$server}"},
{:match => /assets\/itsium\-logo\@2x\.jpg/, :replace => "itsium-logo@2x.jpg"}
]



# script_rep_pattern 
#    only :match and :replace work for js file 
#    to dl file from js file use : mkdir_and_dl("assets/red.png", $server)
#    - when :match is set every match will be replaced with :replace   

$script_rep_pattern = [
{:match => /\"http:\/\/\"\+window\.location\.host/, :replace => "'" + $server_dest[0, ($server_dest.size - 1)] + "'"},
{:match => /serverUrl:""/, :replace => "serverUrl:\"" + $server_dest + "\""},
{:match => /\/assets\/red.png/, :replace => "assets/red.png"},
{:match => /\/assets\/green.png/, :replace => "assets/green.png"},
{:match => /\/assets\/blue.png/, :replace => "assets/blue.png"},
{:match => /\/assets\/about-logo.png/, :replace => "assets/about-logo.png"}
]
$index_rep_pattern = [
{:match => /users\/status\.json[^\"]*/, :replace => "#{$server_dest}users/status.json"}
]
# END GLOBAL VAR


# Remove / of node
def remove_slash(node, str = 'href')
	url = node.attr(str)
	node.attributes[str].value = url[1,url.size]
end

# Download file and put it to right place
# 	file : filename example : assets/image/toto.png
# 	url_pref : url_prefix example : http://toto.com/
# 	dl_pref : localpath_prefix example : my_project

def mkdir_and_dl(file, url_pref, dl_pref = "")
	file = file[1,file.size] if file[0] == "/"
	 # puts "[DEBUG] file = #{file}, pref = #{url_pref}"
	path = "#{dl_pref}#{File.dirname(file)}"
	`mkdir -p #{path}`
	puts "[DEBUG] wget -c -P #{path} -nv #{url_pref}#{file}"
	`wget -c -P #{path} -nv #{url_pref}#{file}`
end

def modify_file_with_pattern(file_path, pat_array)
	# puts "[DEBUG] in modify_file_with_pattern file_path = #{file_path}, pwd = #{Dir.pwd} open = #{$proj_path}/#{file_path}"

	text = File.read("#{$proj_path}/#{file_path}")
	pat_array.each do |rp|
		# debugger
		if rp[:dl] == true
			text.scan(rp[:url_patern]).each do |res|
				if res.class == String
					file = res
				else
					file = res.first
				end
				if rp[:dl_prefix]
					mkdir_and_dl(file, rp[:url_prefix], rp[:dl_prefix])
				else
					mkdir_and_dl(file, rp[:url_prefix])
				end
			end
		end
		if rp.has_key?(:replace)
			text.gsub!(rp[:match], rp[:replace])
		else
			text.gsub!(rp[:match]) { |str|
				str.gsub!(/[\"\']/, "")
				if str[0, 5] == "url(/"
					str.sub!("/", "")
				end
				str
			}
		end
	end
	File.open("#{$proj_path}/#{file_path}", "w") { |f| f.write(text) }
end

def replace_file_with_pattern(file_path, pat_array)

	text = File.read("#{$proj_path}/#{file_path}")

	pat_array.each do |rp|
		text.gsub!(rp[:match], rp[:replace])
	end

	File.open("#{$proj_path}/#{file_path}", "w") { |f| f.write(text) }

end

`mkdir -p #{$proj}`
Dir.chdir $proj
$proj_path = Dir.pwd

doc = Nokogiri::HTML(open($server))
# debugger
# Remove / from cache manifest
doc.css('html').first.attributes['manifest'].remove

# Remove / from css and download files
doc.css('link').each do |link|
	if link.attr('href')[0] == "/"
		# puts "LINK = #{link.attr('href')}"
		remove_slash(link)
		mkdir_and_dl(link.attr('href'), $server)
		modify_file_with_pattern(link.attr('href'), $css_rep_pattern) if link.attr('rel') == "stylesheet" || link.attr('type') == "text/css"
	end
end

doc.css('script').each do |link|
	if link.attr('src') && link.attr('src')[0] == "/"
		# puts "LINK = #{link.attr('href')}"
		remove_slash(link, 'src')
		mkdir_and_dl(link.attr('src'), $server)
		mkdir_and_dl("assets/red.png", $server)
		mkdir_and_dl("assets/green.png", $server)
		mkdir_and_dl("assets/blue.png", $server)
		mkdir_and_dl("assets/about-logo.png", $server)
	  replace_file_with_pattern(link.attr('src'), $script_rep_pattern) if link.attr('type') == "text/javascript"
	end
end

doc.css('script').first.remove

f = open('index.html', 'w')
doc.write_to(f)
f.close

replace_file_with_pattern('index.html', $index_rep_pattern)
