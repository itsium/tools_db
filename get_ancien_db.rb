#!/usr/bin/env ruby
# encoding: UTF-8

require 'nokogiri'
require 'open-uri'
require 'net/http'

#http://anciens2.epitech.eu/aff_ancien/$i

_NOM = nil
_PRENOM = nil
_PROMO = nil
_DIPLOME = []
_EXP = []
_IMG_URL = nil
_IMG = nil

doc = Nokogiri::HTML(open('http://anciens2.epitech.eu/aff_ancien/722'))

doc.search(".titre_couleur").each do |d|
  _NOM = d.next.next.text.strip! if d.child.text == "Nom"
  _PRENOM = d.next.next.text.strip! if d.child.text == "Prénom"
  _PROMO = d.next.next.text.strip! if d.child.text == "Promo"
  if d.child.text == "Diplôme(s)"
    d.parent.parent.xpath('tr').each do |t|
      _DIPLOME << t.children.text.strip! if t.children && t.children.text.strip! != "Diplôme(s)" && t.children.text.strip! != ""
    end
  end
end

_IMG = doc.search("#avatar").attribute('src').to_s
if _IMG == "/img/avatars/vide.jpg"
  _IMG = nil 
else
  _IMG_URL = "http://anciens2.epitech.eu#{_IMG}"
end

unless _IMG.nil?
  Net::HTTP.start("anciens2.epitech.eu") do |http|
      resp = http.get(_IMG)
      open(_IMG[1,_IMG.length], "wb") do |file|
          file.write(resp.body)
      end
  end
end

doc.search(".groCouleur").last.parent.parent.next.search("tr").each do |ex|
  if ex.search('td').at(2).text.strip! != nil
    _EXP << {:post => ex.search('td').at(0).text.strip!,
             :debut => ex.search('td').at(1).text.strip!,
             :fin => ex.search('td').at(2).text.strip!,
             :sociel => ex.search('td').at(3).text.strip!
            }
  end
end


puts "NOM : #{_NOM} PRENOM : #{_PRENOM} PROMO : #{_PROMO}"
_DIPLOME.each do |d|
  puts "DIPLOME : #{d}"
end

_EXP.each do |e|
  puts "EXP : #{e[:post]} #{e[:sociel]} #{e[:debut]} - #{e[:fin]}"
end
puts "IMG : #{_IMG}"


