#!/usr/bin/env ruby
#encoding: utf-8

require 'nokogiri'
require 'open-uri'
require 'sqlite3'
require 'digest/sha1'
require 'debugger'
require 'mail'
require 'net/smtp'

$BASE_URL = "http://www.damai.cn/"
$ADMIN = '65465706@qq.com'
$MAIL_TO = "65465706@qq.com,86769104@qq.com,1637270943@qq.com,969008809@qq.com,595018393@qq.com,2647422702@qq.com"

smtp = { :address => 'smtp.gmail.com',
  :port => 587,
  :domain => 'gmail.com',
  :user_name => 'damil.itsium@gmail.com',
  :password => 'bitebite',
  :authentication       => "plain",
  :enable_starttls_auto => true,
  :openssl_verify_mode => 'none' }
Mail.defaults { delivery_method :smtp, smtp }



# $database = SQLite3::Database.new( "daimai.cn.db" )
$database = SQLite3::Database.new( "/home/ubuntu/daimai.cn.db" )
$database.execute("CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY AUTOINCREMENT, parsetime DATETIME, sha1 VARCHAR(255), html TEXT)")

# $curent_proxy = 'http://190.211.132.33:8080'


def get_last_id
        id = $database.execute("SELECT id FROM events ORDER BY id DESC LIMIT 1")
        (id.nil? or id.last.nil? )? -1 : id.join
end

def verify_sha1(sha1, id)
        sha1 == $database.execute("SELECT sha1 FROM events WHERE id = ?", id).join
end

def diff_and_mail(cur_html, old_html)
#       puts "[INFO] NEW CONTENTS"
        mail_body = ""
        cur_arr = cur_html.css(".item tr").to_a
        old_arr = old_html.css(".item tr").to_a
        cur_hash = {}
        old_hash = {}
        cur_arr.each do |doc|
                cur_hash[doc.css(".name").text] = doc
        end
        old_arr.each do |doc|
                old_hash[doc.css(".name").text] = doc
        end
        old_hash.each do |k, doc|
          if cur_hash.has_key?(k)
            cur_hash.delete(k)
            old_hash.delete(k)
          end
        end
        return false if cur_hash.count == 0 && old_hash.count == 0
        cur_hash.each do |k, doc|
            name = doc.css(".name").text
            time = doc.css(".time").text
            venue = doc.css(".venue").text
            price = doc.css(".price").text
            link = doc.css(".name a").attr("href").text unless doc.css(".name a").count
            link = "" if link.nil?
            mail_body << "[+] | <a href='#{$BASE_URL+link}'>#{name}</a> | #{time} | #{venue} | #{price}<BR>"
        end
        # old_hash.each do |k, doc|
        #     name = doc.css(".name").text
        #     time = doc.css(".time").text
        #     venue = doc.css(".venue").text
        #     price = doc.css(".price").text
        #     link = doc.css(".name a").attr("href").text
        #     mail_body << "[-] | <a href='#{$BASE_URL+link}'>#{name}</a> | #{time} | #{venue} | #{price}<BR>"
        # end
        return false if cur_hash.count == 0# && old_hash.count == 0
        mail = Mail.new do
          from 'daimai_itsium@163.com'
          to $MAIL_TO
          subject '大麦网更新！'
          html_part do
            content_type 'text/html; charset=UTF-8'
            body mail_body
          end
        end
        mail.deliver!
        #puts mail_body
end

def dump_data(url)
        doc = Nokogiri::HTML(open(url, 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21'))
        $html = doc.css(".item.mt20")
        $html.each do |sec|
          $html.delete sec if sec != nil && (sec.css(".title").text =~ /度假休闲/ || sec.css(".title").text =~ /周边产品/)
        end
        $parsetime = Time.now.strftime("%d-%m-%Y %H:%M:%S")
        $sha1 = Digest::SHA1.hexdigest($html.css(".name").to_s)
        last_id = get_last_id
        if (last_id == -1) || ( ! verify_sha1($sha1, last_id))
                diff_and_mail($html, Nokogiri::HTML($database.execute("SELECT html FROM events WHERE id = ?", last_id).join)) if (last_id != -1)
#               puts "[INFO] SAVE QUERY TO DB"
                $database.execute("INSERT into events (parsetime, sha1, html) VALUES (?, ?, ?)", $parsetime, $sha1, $html.to_s)
        end
end


begin
  dump_data("http://www.damai.cn/alltickets_852.html") #beijing
rescue Exception => e
        mail = Mail.new do
          from 'daimai_itsium@163.com'
          to $ADMIN
          subject '[ALERT]... crowler script error !'
          html_part do
            content_type 'text/html; charset=UTF-8'
            body "[ERR] error: #{e.inspect.gsub(/[<>]/, "")}"
          end
        end
        mail.deliver!
        puts "[ERR] error: #{e.inspect}"
end
