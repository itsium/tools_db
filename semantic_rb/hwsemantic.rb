#!/usr/bin/env ruby
require 'rdf'
require 'linkeddata'

graph = RDF::Graph.load("http://dbpedia.org/resource/Elvis_Presley")
puts graph.size

query = RDF::Query.new({
  :person => {
    RDF::URI("http://dbpedia.org/ontology/type") => :type,
    RDF::URI("http://dbpedia.org/ontology/deathDate") => :deathDate
  }
})

results = query.execute(graph)
puts results.first[:type].to_s
