#!/usr/bin/env ruby
$LOAD_PATH << 'vendor/cache'
require 'nokogiri'
require 'open-uri'
require 'sqlite3'

// https://www.lafraise.com/gallery/all/page/1?ajax=true

$database = SQLite3::Database.new( "lafraise.db" )
$database.execute("CREATE TABLE IF NOT EXISTS tshirt_db (id INTEGER PRIMARY KEY AUTOINCREMENT, article_id INT NOT NULL UNIQUE, title VARCHAR(255), author VARCHAR(255), author_link VARCHAR(255), desc TEXT)")
$database.execute("CREATE TABLE IF NOT EXISTS images_db (id INTEGER PRIMARY KEY AUTOINCREMENT, imange_id INT, image_lpath VARCHAR(255), image_rpath VARCHAR(255))")

$curent_proxy = 'http://190.211.132.33:8080'


def get_last_id
	id = $database.execute("SELECT id FROM tshirt_db ORDER BY id DESC LIMIT 1")
	(id.nil? or id.last.nil? )? 1 : id.last.last
end



def dump_data(url)
	
	@img_url = []

	doc = Nokogiri::HTML(open(url, :proxy => $curent_proxy, 'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21'))
	doc.xpath('//*[@id="singleArticleInfo"]').each do | link |
		@title = link.at_css("h1").content
		@author = link.at_css("h2").content
		@author_link = link.at_css("a")[:href]
	end
	doc.css('li[id^="thumbnail_"]').each do | link |
		@img_url << link.at_css("img")[:src].sub( "thumb", "large" )
	end

	doc.xpath('//*[@id="articleFamilyDescription"]').each do | link |
		@desc = link.content
	end
	$database.execute("INSERT into tshirt_db (article_id, title, author, author_link, desc) VALUES (?, ?, ?, ?, ?)", $last_id, @title, @author, @author_link, @desc)

	@img_url.each do | url |
		system("wget -c -q -P 'photos/' " + url)
		@img_lpath = url.sub("http://cache.spreadshirt.net/users/563000/562298/articleFamilies/1/", "photos/")
		$database.execute("INSERT into images_db (imange_id, image_lpath, image_rpath) VALUES (?, ?, ?)", $last_id, @img_lpath, url)
	end 
end

$last_id = get_last_id
$last_id = 1 if $last_id == 0
puts "id = " + $last_id.to_s

# dump_data("http://www.lafraise.com/Article/index/id/1")
while ($last_id <= 1197) do
	puts "=========== Processing " + $last_id.to_s + " / 1997 record. ==========="
	begin
  		dump_data("http://www.lafraise.com/Article/index/id/" + $last_id.to_s)
  	rescue SQLite3::ConstraintException => sqle
  		$last_id += 1
  	rescue OpenURI::HTTPError => httpe
  		change_proxy
 	rescue Exception => e 
  		puts "[ERR] error getting page : " + "http://www.lafraise.com/Article/index/id/" + $last_id.to_s + "\n" + e.inspect
	else
		$last_id+=1
	end
end
