#!/bin/sh

# funciotns 

promptyn () {
    while true; do
        read -p "$1 " yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer y or n.";;
        esac
    done
}

install_module() {
	cd "$EXTRA_DIR/ext/$1"
	echo "[+] Installing ruby module : $1"
	ruby extconf.rb && make && make install
	if [ $? != "0" ] ; then
  		echo "[Error] Complication error."
  		if promptyn "Do you want to continu anyway?"; then
  	  		echo "Resume job."
		else
    		exit 1	
		fi
	fi
}

# main

if [ `id -u` != "0" ]; then
	echo "[+] You have to run this script with root privilege !"
	exit 1
fi

curl11 -v 2> /dev/null
if [ $? != "2" ]
	echo "[+] Please install curl before using this script !"
        exit 1
fi

CUR_LINK=`curl http://www.ruby-lang.org/fr/downloads/ 2> /dev/null | grep ruby.*tar.gz | head -n 1 | sed "s/.*\"\([^\"].*\)\".*/\1/g"`
CUR_FILE=`echo $CUR_LINK | cut -d "/" -f 7`
echo "[+] Download Ruby $CUR_FILE"
cd /usr/local/src/
wget $CUR_LINK
echo "[+] Umcompress tarball to /usr/local/src/"
tar -zxvf $CUR_FILE 2> /dev/null > /dev/null
if [ $? != "0" ] ; then
  	echo "[Error] no file found !"
	exit 1
fi
rm -rf $CUR_FILE
EXTRA_DIR="/usr/local/src/"`ls -p | grep "/" | grep ruby- | head -n 1`
apt-get -v 2> /dev/null > /dev/null
if [ $? = "0" ] ; then
	echo "[+] apt-get found, installing dependancies !"
	apt-get -y install build-essential libssl-dev libreadline6-dev zlib1g-dev libyaml-dev libsqlite3-dev
	HAS_APT_GET='1'	
fi

if [ $HAS_APT_GET ] ; then
	cd $EXTRA_DIR
	echo "[+] Compiling ruby please wait !"
	./configure --prefix=/usr/local --with-openssl-dir=/usr --with-readline-dir=/usr --with-zlib-dir=/usr && make && make install 
	install_module psych
	install_module curses
	install_module bigdecimal
	install_module dl
	install_module dbm
	install_module digest
	install_module fcntl
	install_module gdbm
	install_module iconv
	install_module json
	install_module openssl
	install_module pty
	install_module readline
	install_module ripper
	install_module sdbm
	install_module socket
	install_module stringio
	install_module strscan
	install_module syck
	install_module syslog
	install_module tk
	install_module zlib
	
	gem install sqlite3 --no-ri --no-rdoc

	echo "[+] You can now \"gem install rails\" ! Enjoy !"
else
	echo "[+] Your OS : `uname -o` is not supported yet ! Please contact myass@isaway.com"
fi
